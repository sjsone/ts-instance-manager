# Instance Manager

An instance of the InstanceManager is exported as `InstanceManager` and the class as `InstanceManagerClass`

## Methods

### `.isManaged(identifier: string): bool`

Returns if a Singleton or Managed Class with the provided `identifier` is managed.

### `instance(identifierOrObjectOrClass: any): Object`

If an Singleton Instance for the provided identifier, instance or class is found it gets returned. Otherwise `undefined` is returned.

## Decorators

### Singleton
`@Singleton(entryIdentifier?: string, attachInstaceMethod = false)` 

The `entryIdentifier` is used to reference the class in all other InstanceManager methods. If no `entryIdentifier` is provided the class name is used. 

If `attachInstaceMethod` is `true` then the class gets a static `Instance` method attached to.

**Every Singleton has to be instantiated once before it can be used in an injected property**

To get an Instance of the Singleton use `InstanceManager.instance(identifierOrObjectOrClass)` method or the attached  `.Instance()`  method.

### Managed
`@Managed(entryIdentifier?: string)`

### Inject

`@Inject(identifierOrObjectOrClass)`

 `identifierOrObjectOrClass` can be either an `entryIdentifier` , a class instance or the class itself.

#### Example:

```typescript
class ExampleClass {
  	// you can also use: @Inject('OneTest')
  	// or: @Inject(OneTest)
    @Inject(one)
    protected oneTest = <OneTest><any>undefined 

    getTest() {
        return this.oneTest.getTest()
    }
}
```

## Extending InstanceManager

The `resetInstanceManagerInstanceTo` function the current **Instance Manager** instance can be replaced. This is needed if an extended class of the InstanceManager should be used.

### Example

```typescript
import { InstanceManagerClass, resetInstanceManagerInstanceTo } from 'ts-instance-manager'

class InstanceManager extends InstanceManagerClass {
    doSomeThing() {
        return 'something'
    }
}

const InstanceManagerInstance = new InstanceManager()
resetInstanceManagerInstanceTo(InstanceManagerInstance)
export { InstanceManagerInstance as InstanceManager, InstanceManager as InstanceManagerClass }
```

# Example 

```typescript
import { InstanceManager, Managed, Singleton, Inject } from "./InstanceManager";

@Singleton('TestOne')
class OneTest {
    test: string
    constructor(test: string) {
        this.test = test
    }

    getTest() {
        return this.test
    }
}

const one = new OneTest('one')
const two = new OneTest('two')

class ExampleClass {
  	// you can also use: @Inject('TestOne')
  	// or: @Inject(OneTest)
  	// or: @Inject(two)
    @Inject(one)
    protected oneTest = <OneTest><any>undefined 

    getTest() {
        return this.oneTest.getTest()
    }
}

// returns true
console.log(one === two)

// returns true 
console.log(two === InstanceManager.instance(OneTest))

// returns true
console.log(InstanceManager.isManaged('TestOne'))

const inejctExample = new ExampleClass()
// returns 'one'
console.log(managedTest.getTest())

```

