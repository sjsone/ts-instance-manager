import { TestGroup } from "yaf-test"
import { Inject } from "./Decorator/Inject"
import { Singleton } from "./lib"

@Singleton('ServiceTest')
class TestService {
    test: string
    constructor(test: string) {
        this.test = test
    }
    getTest = () => this.test
}

const one = new TestService('one')
const two = new TestService('two')

class Service {
    @Inject(TestService)
    public testServiceClass: TestService = <TestService><any>undefined
    @Inject('ServiceTest')
    public testServiceIdentifer: TestService = <TestService><any>undefined
    @Inject(one)
    public testServiceInstance: TestService = <TestService><any>undefined
    runClass = () => this.testServiceClass.getTest()
    runIdentifier = () => this.testServiceIdentifer.getTest()
    runInstance = () => this.testServiceInstance.getTest()
}
const service = new Service()


TestGroup.Area('Singleton', test => {
    test('Compare instances', one).is(two)
    test('Compare method results', one.getTest()).is(two.getTest())    
})

TestGroup.Area('Inject', test => {
    test('Class', service.testServiceClass).is(one)
    test('Identifier', service.testServiceIdentifer).is(one)
    test('Instance', service.testServiceInstance).is(one)  
})

