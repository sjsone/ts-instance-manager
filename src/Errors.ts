import { Entry } from "./EntryManager";

export class EntryAlreadyExistsError extends Error {
    protected newEntry: Entry
    protected existingEntry: Entry

    constructor(key: string, newEntry: Entry, existingEntry: Entry) {
        super()
        this.message = `Entry with key "${key}" already exists`
        this.newEntry = newEntry
        this.existingEntry = existingEntry
    }
}

export class EntryDoesNotExistError extends Error {
    protected updatedEntry?: Entry

    constructor(key: string, updatedEntry?: Entry) {
        super()
        this.message = `Entry with key "${key}" does not exist`
        this.updatedEntry = updatedEntry
    }
}