import { InstanceManager, InstanceManagerClass } from "../InstanceManager"

export function Inject(instanceOrClassOrIdentifier: string): (target: any, propertyKey: string) => void
export function Inject(instanceOrClassOrIdentifier: Function | Object): (target: any, propertyKey: string) => void
export function Inject(instanceOrClassOrIdentifier: any) {
    const identifier = InstanceManagerClass.getEntryIdentifier(instanceOrClassOrIdentifier)

    return function (target: any, propertyKey: string) {
        let instance: any = undefined
        let firstTry = true

        Object.defineProperty(target, propertyKey, {
            get: () => {
                if (firstTry) {
                    const entry = InstanceManager.getEntry(identifier)
                    if (entry) {
                        instance = entry.instance
                    }
                    firstTry = false
                }
                return instance
            },
            set: (value: any) => { }
        })
    }
}