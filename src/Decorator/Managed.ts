import { EntryManager } from "../EntryManager"
import { Decorator, InstanceManager, InstanceManagerClass } from "../InstanceManager"

export const Managed: Decorator = (entryIdentifier?: string) => (constructor: Function) => {
    const identifier: string = entryIdentifier !== undefined ? entryIdentifier : constructor.name

    InstanceManagerClass.attachIdentifierToConstructor(constructor, identifier)

    EntryManager.setEntry(identifier, InstanceManager.createEmptyEntry(Managed))

    return new Proxy(constructor, {
        construct: function (target: any, argArray: any[], newTarget: Function) {
            return new Proxy(Reflect.construct(target, argArray, newTarget), InstanceManager.getManagedHandler())
        }
    })
}