import { EntryManager } from "../EntryManager"
import { InstanceManagerClass, InstanceManager, Decorator } from "../InstanceManager"

export const Singleton: Decorator = (entryIdentifier?: string, attachInstaceMethod = false) => (constructor: any) => {
    const identifier: string = entryIdentifier !== undefined ? entryIdentifier : constructor.name

    InstanceManagerClass.attachIdentifierToConstructor(constructor, identifier)

    if (attachInstaceMethod) {
        constructor.Instance = () => InstanceManager.instance(identifier)
    }

    EntryManager.setEntry(identifier, InstanceManager.createEmptyEntry(Singleton))

    return new Proxy(constructor, InstanceManager.getSingletonHandler(constructor))
}