import { EntryAlreadyExistsError, EntryDoesNotExistError } from "./Errors"

export interface Entry {
    instance: any
}

class EntryManager {
    protected entries: { [key: string]: Entry } = {}

    hasEntry(key: string) {
        return this.getEntry(key) !== undefined
    }

    getEntry(key: string, strict: boolean = false) {
        if (strict && !this.hasEntry(key)) {
            throw new EntryDoesNotExistError(key)
        }
        return this.entries[key]
    }

    updateEntry(key: string, entry: Entry) {
        if (!this.hasEntry(key)) {
            throw new EntryDoesNotExistError(key, entry)
        }
        this.entries[key] = entry
    }

    setEntry(key: string, entry: Entry) {
        if (this.hasEntry(key)) {
            throw new EntryAlreadyExistsError(key, entry, this.getEntry(key))
        }
        this.entries[key] = entry
    }
}

const EntryManagerInstance = new EntryManager()
export { EntryManagerInstance as EntryManager }