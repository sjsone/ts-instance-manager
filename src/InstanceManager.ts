import { Entry, EntryManager } from "./EntryManager"

class InstanceManager {

    protected managedHandler: () => any = function () {
        return {}
    }

    static PROP_ENTRY_IDENTIFIER = '__instance_manager_identifier'

    static getEntryIdentifier(value: any): string {
        switch (typeof value) {
            case 'object': return value.constructor[InstanceManager.PROP_ENTRY_IDENTIFIER]
            case 'function': return value[InstanceManager.PROP_ENTRY_IDENTIFIER]
            default: return value
        }
    }

    static attachIdentifierToConstructor(constructor: Function, identifier: string) {
        Object.defineProperty(constructor, InstanceManager.PROP_ENTRY_IDENTIFIER, {
            enumerable: false,
            writable: false,
            value: identifier
        });
    }

    isManaged(identifier: string) {
        return EntryManager.hasEntry(identifier)
    }

    getEntry(key: string, strict: boolean = false) {
        return EntryManager.getEntry(key, strict)
    }

    instance(identifierOrObjectOrClass: string): Object
    instance<T>(identifierOrObjectOrClass: new (...arg: any[]) => T): T
    instance(identifierOrObjectOrClass: any): Object {
        const identifier = InstanceManager.getEntryIdentifier(identifierOrObjectOrClass)
        const entry = EntryManager.getEntry(identifier)
        return entry !== undefined ? entry.instance : undefined
    }

    setManagedHandler(managedHandler: () => any) {
        this.managedHandler = managedHandler
    }

    getManagedHandler() {
        return this.managedHandler()
    }

    protected createSingletonEntry(target: any, argumentArray: any[], newTarget: Function): Entry {
        return {
            instance: new Proxy(Reflect.construct(target, argumentArray, newTarget), this.getManagedHandler())
        }
    }

    createEmptyEntry(type?: Decorator): Entry {
        return {
            instance: undefined
        }
    }

    getSingletonHandler(constructor: Function) {
        return {
            construct: (target: any, argArray: any[], newTarget: Function) => {
                // TODO: handle extended singleton based on configuration
                if (target.prototype !== newTarget.prototype) {
                    return Reflect.construct(target, argArray, newTarget)
                }
                const key = (<any>constructor)[InstanceManager.PROP_ENTRY_IDENTIFIER]
                if(EntryManager.getEntry(key).instance === undefined) {
                    EntryManager.updateEntry(key, this.createSingletonEntry(target, argArray, newTarget))
                }
                return EntryManager.getEntry(key).instance
            }
        }
    }
}

let InstanceManagerInstance = new InstanceManager()
export { InstanceManagerInstance as InstanceManager, InstanceManager as InstanceManagerClass }

export const resetInstanceManagerInstanceTo = (instance: any) => {
    InstanceManagerInstance = instance
}

export type Decorator = (entryIdentifier?: string) => (constructor: Function) => any