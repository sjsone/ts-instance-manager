import { InstanceManager, Managed, Singleton, Inject } from "./lib";

@Singleton()
class TestService {
    test: string
    constructor(test: string) {
        this.test = test
    }

    getTest() {
        return this.test
    }
}

const one = new TestService('one')
const two = new TestService('two')

@Managed('m')
class ManagedTest {

    @Inject(one)
    protected oneTest?: TestService

    getTest() {
        return this.oneTest?.getTest()
    }
}

console.log(one === two)
console.log(two === InstanceManager.instance(TestService))
console.log(InstanceManager.isManaged('m'))

const managedTest = new ManagedTest()
console.log("getTest", managedTest.getTest())

class InjectTest {
    @Inject(one)
    protected oneTest = <TestService><any>undefined

    getTest() {
        return this.oneTest.getTest()
    }
}

const injectTest = new InjectTest()
console.log(injectTest.getTest())